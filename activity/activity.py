# S01 Activity
# 1. Create 5 variables and output them in the command prompt in the following format: 
# "I am < name (string)> , and I am < age (integer)> years old, I work as a < occupation (string)> , and my rating for < movie (string)> is < rating (decimal)> %"

name = "John"
age = 19
occupation = "Student"
movie = "Interstellar"
rating = 100.00

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}%")

# 2. Create 3 variables, num1, num2, and num3
num1 = 1
num2 = 2
num3 = 3

    # a. Get the product of num1 and num2
print(num1 * num2)

    # b. Check if num1 is less than num3
print(num1 < num3)

    # c. Add the value of num3 to num2
num2 += num3